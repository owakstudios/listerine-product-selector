var app=angular.module('productSelector', [
	'ngAnimate',
	'ngRoute',
	'angulartics',
	'angulartics.google.analytics',
	'angulike'
	]);

app.controller('AppController',['$scope','$filter','$location','$rootScope',function($scope,$filter,$location,$rootScope){

	$scope.pageTitle= 'LISTERINE® sabor suave, sabor intenso y blanqueamiento';
	$scope.meta_title='';
	$scope.meta_description='';
	$scope.meta_url='';
	$scope.meta_keywords='';
	$scope.meta_img_url='';

	/*
	$scope.pageTitle= '';
	$scope.meta_title='';
	$scope.meta_description='';
	$scope.meta_url='';
	$scope.meta_keywords='';
	*/


	$scope.tipos=[
	{
		id:'LISTERINE-solucion',
		share_id:'LISTERINE-Solucion',
		name:'Solución',
		abstract:'Cepillarse los dientes solo limpia el 60%* de tu boca, permitiendo que la placa bacteriana se desarrolle en esas áreas difíciles de alcanzar.',
		por_que_usar:'Los dentistas e higienistas recomiendan hacer algo más que simplemente cepillarse: usar la seda dental y un enjuague bucal como LISTERINE® Solución. Utilízalo 2 veces al día y te darás cuenta que:',
		grupo:'Beneficios Básicos',
		sin_alcohol:false,
		ecias_saludables:true,
		reducir_placa:true,
		combate_99_mal_aliento:true,
		aliento_fresco:true,
		control_sarro:false,
		fortalecer_dientes:false,
		maximos_beneficios:false,
		blanquea:false,
		tipo:'normal',
		sabor:'Astringente',
		color:'#f1e6c4',
		color_2:'#d0ab3a',
		bitly:'http://bit.ly/1bvgkSL',
		link_exito:'http://www.exito.com/products/0000472243684721/Original+pet?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Solucion',
		link_la14:'http://www.la14.com/Tiendala14/Products/base_catalog/PID-499.aspx?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Solucion',
		link_larebaja:'http://www.larebajavirtual.com/catalogo/index/verproducto/producto/LISTERINE-ORIGINAL/productoId/14619/divId?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Solucion'
	},
	{
		id:'LISTERINE-freshburst',
		share_id:'LISTERINE-Freshburst',
		name:'FreshBurst',
		abstract:'Después de cepillarse los dientes 2 veces al día los dientes pueden presentar hasta un 69% de placa*',
		por_que_usar:'Es por esto que los dentistas e higienistas recomiendan hacer algo más que simplemente cepillarse: usar la seda dental y un enjuague bucal como LISTERINE® FreshBurst. Utilízalo 2 veces al día y te darás cuenta que:',
		grupo:'Beneficios Básicos',
		sin_alcohol:false,
		ecias_saludables:true,
		reducir_placa:true,
		combate_99_mal_aliento:true,
		aliento_fresco:true,
		control_sarro:false,
		fortalecer_dientes:false,
		maximos_beneficios:false,
		blanquea:false,
		tipo:'normal',
		sabor:'Menta Fresca',
		color:'#b3ddd1',
		color_2:'#018e64',
		bitly:'http://bitly.com/1F4dbau',
		link_exito:'http://www.exito.com/products/0000506026774413/Fresh+burst?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&tm_term=Listerine-FreshBurst',
		link_la14:'http://www.la14.com/Tiendala14/Products/base_catalog/PID-164465.aspx?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-FreshBurst',
		link_larebaja:'http://www.larebajavirtual.com/catalogo/index/verproducto/producto/LISTERINE-FRESHBURST/productoId/28316/divId/?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-FreshBurst'
	},
	{
		id:'LISTERINE-cool-mint',
		share_id:'LISTERINE-Cool-Mint',
		name:'Cool Mint',
		abstract:'Cepillarse los dientes solo limpia el 60%* de tu boca, permitiendo que la placa bacteriana se desarrolle en esas áreas difíciles de alcanzar.',
		por_que_usar:'Utiliza LISTERINE® Cool Mint 2 veces al día y te darás cuenta que:',
		grupo:'Beneficios Básicos',
		sin_alcohol:false,
		ecias_saludables:true,
		reducir_placa:true,
		combate_99_mal_aliento:true,
		aliento_fresco:true,
		control_sarro:false,
		fortalecer_dientes:false,
		maximos_beneficios:false,
		blanquea:false,
		tipo:'normal',
		sabor:'Menta Helada',
		color:'#b8e7ed',
		color_2:'#11afc3',
		bitly:'http://bitly.com/1KdNulO',
		link_exito:'http://www.exito.com/products/0000472227684710/Listerine+coolmint?utm_source=website&tm_medium=LISTERINE.co&tm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Cool-Mint',
		link_la14:'http://www.la14.com/Tiendala14/Products/base_catalog/PID-160452.aspx?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Cool-Mint',
		link_larebaja:'http://www.larebajavirtual.com/catalogo/index/verproducto/producto/LISTERINE-COOL-MINT/productoId/20529/divId/?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Cool-Mint'
	},
	{
		id:'LISTERINE-zero-menta-verde',
		share_id:'LISTERINE-Zero-menta-verde',
		name:'ZERO™ Menta Verde',
		abstract:'El nuevo LISTERINE® ZERO™ Menta Verde SIN ALCOHOL, es más suave, pero igualmente efectivo.',
		por_que_usar:'Usándolo 2 veces al día, se ha comprobado que LISTERINE® Control Cálculo:',
		grupo:'sin Alcohol',
		sin_alcohol:true,
		ecias_saludables:true,
		reducir_placa:true,
		combate_99_mal_aliento:true,
		aliento_fresco:true,
		control_sarro:false,
		fortalecer_dientes:false,
		maximos_beneficios:false,
		blanquea:false,
		tipo:'normal',
		sabor:'Menta Verde',
		color:'#d5eac9',
		color_2:'#73b94a',
		bitly:'http://bitly.com/1OXGU9B',
		link_exito:'',
		link_la14:'',
		link_larebaja:''
	},
	{
		id:'LISTERINE-zero',
		share_id:'LISTERINE-Zero',
		name:'ZERO™',
		abstract:'Para aquellos que prefieren las cosas más suaves en la vida, la fórmula de LISTERINE® ZERO™ sin ALCOHOL, te da la limpieza profunda de LISTERINE®, pero con un sabor menos intenso.',
		por_que_usar:'Usándolo 2 veces al día, se ha comprobado que LISTERINE® ZERO™',
		grupo:'sin Alcohol',
		sin_alcohol:true,
		ecias_saludables:true,
		reducir_placa:true,
		combate_99_mal_aliento:true,
		aliento_fresco:true,
		control_sarro:false,
		fortalecer_dientes:false,
		maximos_beneficios:false,
		blanquea:false,
		tipo:'normal',
		sabor:'Menta Suave',
		color:'#b3dada',
		color_2:'#008484',
		bitly:'http://bitly.com/1zKu2gp',
		link_exito:'http://www.exito.com/products/0001172071062622/Listerine+zero?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Zero',
		link_la14:'http://www.la14.com/Tiendala14/Products/base_catalog/PID-1986769.aspx?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Zero',
		link_larebaja:'http://www.larebajavirtual.com/catalogo/index/verproducto/producto/LISTERINE-ZERO/productoId/97331/divId/?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Zero'
	},
	{
		id:'LISTERINE-control-calculo',
		share_id:'LISTERINE-Control-Calculo',
		name:'Control Calculo',
		abstract:'LISTERINE® Protección Dientes y Encías está especialmente formulado con flúor para fortalecer el esmalte dental y proteger contra la caries. Además se ha demostrado que previene y reduce la placa, proporcionando protección para dientes y encías sanos.',
		por_que_usar:'Usándolo 2 veces al día, se ha comprobado que LISTERINE® Protección Dientes y Encías:',
		grupo:'Beneficios Adicionales',
		sin_alcohol:false,
		ecias_saludables:true,
		reducir_placa:true,
		combate_99_mal_aliento:true,
		aliento_fresco:true,
		control_sarro:true,
		fortalecer_dientes:false,
		maximos_beneficios:false,
		blanquea:false,
		tipo:'normal',
		sabor:'Menta Fría',
		color:'#c0ccdb',
		color_2:'#2d5586',
		bitly:'http://bitly.com/1Pm9SuI',
		link_exito:'http://www.exito.com/products/0000054213057364/ENJUAGUE+CONTROL+CALCULO?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Control-Calculo',
		link_la14:'http://www.la14.com/Tiendala14/Products/base_catalog/PID-319876.aspx?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Control-Calculo',
		link_larebaja:'http://www.la14.com/Tiendala14/Products/base_catalog/PID-319876.aspx?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Control-Calculo'
	},
	{
		id:'LISTERINE-proteccion-dyt',
		share_id:'LISTERINE-Proteccion-Dientes-y-Encias',
		name:'Protección Dientes y Encías',
		abstract:'LISTERINE® Protección Dientes y Encías está especialmente formulado con flúor para fortalecer el esmalte dental y proteger contra la caries. Además, como todo enjuague bucal LISTERINE®, alcanza y penetra más profundamente el biofilm.',
		por_que_usar:'Usándolo 2 veces al día, se ha comprobado que LISTERINE® Protección Dientes y Encías:',
		grupo:'Beneficios Adicionales',
		sin_alcohol:false,
		ecias_saludables:true,
		reducir_placa:true,
		combate_99_mal_aliento:true,
		aliento_fresco:true,
		control_sarro:false,
		fortalecer_dientes:true,
		maximos_beneficios:false,
		blanquea:false,
		tipo:'normal',
		sabor:'Menta Fresca',
		color:'#bfe4d1',
		color_2:'#28a363',
		bitly:'http://bitly.com/1OXHhRI',
		link_exito:'http://www.exito.com/products/0000315280281575/Listerine+protecci%C3%B3n+dientes+y+encias?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&tm_campaign=Johnson-and-Johnson&utm_term=Listerine-Proteccion-Dientes-y-Encias',
		link_la14:'http://www.la14.com/Tiendala14/Products/base_catalog/PID-615833.aspx?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Proteccion-Dientes-y-Encias',
		link_larebaja:'http://www.larebajavirtual.com/catalogo/index/verproducto/producto/LISTERINE-DIENTES-ENCIAS/productoId/41020/divId/?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Proteccion-Dientes-y-Encias'
	},
	{
		id:'LISTERINE-cuidado-total',
		share_id:'LISTERINE-Cuidado-Total',
		name:'Cuidado Total',
		abstract:'El enjuague bucal LISTERINE® más avanzado y completo con multi-beneficios para una salud oral completa.',
		por_que_usar:'Al usarlo 2 veces al día, LISTERINE®Cuidado Total actúa de 6 maneras diferentes para mantener tu boca limpia y sana:',
		grupo:'Beneficios Adicionales',
		sin_alcohol:false,
		ecias_saludables:true,
		reducir_placa:true,
		combate_99_mal_aliento:true,
		aliento_fresco:true,
		control_sarro:true,
		fortalecer_dientes:true,
		maximos_beneficios:true,
		blanquea:false,
		tipo:'normal',
		sabor:'Menta Fresca',
		color:'#d7cedf',
		color_2:'#7a5b94',
		bitly:'http://bitly.com/1F4dDWk',
		link_exito:'http://www.exito.com/products/0000253450218442/LISTERINE+CUIDADO+TOTAL?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Cuidado-Total',
		link_la14:'http://www.la14.com/Tiendala14/Products/base_catalog/PID-940229.aspx?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Cuidado-Total',
		link_larebaja:'http://www.larebajavirtual.com/catalogo/index/verproducto/producto/LISTERINE-CUIDADO-TOTAL/productoId/59286/divId/?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Cuidado-Total'
	},
	// {
	// 	id:'LISTERINE-whitening-precepillado',
	// 	share_id:'LISTERINE-Whitening-PreCepillado',
	// 	name:'Whitening Pre-Cepillado',
	// 	abstract:'¡Para una sonrisa 3 veces más blanca! Con tan solo 2 minutos al día, LISTERINE® Whitening Pre-Cepillado blanquea tus dientes en 12 semanas**. ¡Es así de fácil!',
	// 	por_que_usar:'Si lo usas dos veces al día, antes del cepillado, te darás cuenta que:',
	// 	hidrogeno:true,
	// 	fluor:false,
	// 	zinc:false,
	// 	dientes_3x_blancos:true,
	// 	dientes_mas_fuertes:false,
	// 	mantiene_blanco_natural:false,
	// 	mantiene_blanco_profesional:false,
	// 	ecias_saludables:false,
	// 	reducir_placa:false,
	// 	combate_99_mal_aliento:false,
	// 	aliento_fresco:true,
	// 	control_sarro:false,
	// 	blanquea:true,
	// 	tipo:'whitening',
	// 	sabor:'Menta',
	// 	color:'#c7e9f2',
	// 	color_2:'#cd188d',
	// 	bitly:'http://bitly.com/1JoGUsq',
	// 	link_exito:'http://www.exito.com/products/0000658831952307/Listerine-whiitening?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Whitening-Precepillado',
	// 	link_la14:'http://www.la14.com/Tiendala14/Products/base_catalog/PID-765340.aspx?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Whitening-Precepillado',
	// 	link_larebaja:'http://www.larebajavirtual.com/catalogo/index/verproducto/producto/LISTERINE-WHITENING/productoId/53415/divId/?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Whitening-Precepillado'
	// },
	{
		id:'LISTERINE-whitening-2en1',
		share_id:'LISTERINE-Whitening-2en1',
		name:'Whitening Blanquea y Fortalece',
		abstract:'LISTERINE® Whitening Blanquea y Fortalece blanquea tus dientes mientras fortalece el esmalte dental en tan solo 7 días**',
		por_que_usar:'Usándolo 2 veces al día, se ha demostrado que LISTERINE® Whitening Blanquea y Fortalece:',
		hidrogeno:true,
		fluor:true,
		zinc:false,
		dientes_3x_blancos:false,
		dientes_mas_fuertes:true,
		mantiene_blanco_natural:false,
		mantiene_blanco_profesional:false,
		ecias_saludables:false,
		reducir_placa:false,
		combate_99_mal_aliento:false,
		aliento_fresco:true,
		control_sarro:false,
		blanquea:true,
		fortalecer_dientes:true,
		tipo:'whitening',
		sabor:'Menta',
		color:'#bec5df',
		color_2:'#253b93',
		bitly:'http://bitly.com/1KdOtSU',
		link_exito:'http://www.exito.com/products/0001054702004483/List+whitening+blanquea+fortal?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Whitening-2en1',
		link_la14:'http://www.la14.com/Tiendala14/Products/base_catalog/PID-1412964.aspx?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Whitening-2en1',
		link_larebaja:'http://www.larebajavirtual.com/catalogo/index/verproducto/producto/LISTERINE-WHITENING-2-EN-1/productoId/93849/divId/?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Whitening-2en1'
	},
	{
		id:'LISTERINE-whitening-antimanchas',
		share_id:'LISTERINE-Whitening-Antimanchas',
		name:'Whitening Antimanchas',
		abstract:'LISTERINE® Whitening Antimanchas está especialmente formulado para mantener el blanco natural de tus dientes dejándolos más brillantes.',
		por_que_usar:'Usándolo 2 veces al día, se ha demostrado que LISTERINE® Whitening Antimanchas: ',
		hidrogeno:false,
		fluor:false,
		zinc:true,
		dientes_3x_blancos:false,
		dientes_mas_fuertes:false,
		mantiene_blanco_natural:true,
		mantiene_blanco_profesional:true,
		ecias_saludables:true,
		reducir_placa:true,
		combate_99_mal_aliento:true,
		aliento_fresco:true,
		control_sarro:true,
		blanquea:false,
		tipo:'whitening',
		sabor:'Menta Suave',
		color:'#f0badd',
		color_2:'#42b5d3',
		bitly:'http://bitly.com/1OXHGn8',
		link_exito:'http://exito.com/products/0001445040247208/Listerine+Antimanchas&utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Antimanchas',
		link_la14:'http://www.la14.com/Tiendala14/Products/base_catalog/PID-2751656.aspx?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Whitening-Antimanchas',
		link_larebaja:'http://www.larebajavirtual.com/catalogo/index/verproducto/producto/LISTERINE-WHITENING-ANTIMANCHA/productoId/17801/divId/?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Antimanchas'
	},
	{
		id:'LISTERINE-whitening-extreme',
		share_id:'LISTERINE-Whitening-Extreme',
		name:'Whitening Extreme',
		abstract:'LISTERINE® Whitening Extreme está especialmente formulado para pacientes que buscan una sonrisa más blanca en 5 días*',
		por_que_usar:'Para pacientes que además de mantener el blanco, requieren fortalecer el esmalte dental.',
		grupo:'Beneficios Básicos',
		sin_alcohol:false,
		ecias_saludables:false,
		reducir_placa:false,
		combate_99_mal_aliento:false,
		aliento_fresco:false,
		control_sarro:false,
		fortalecer_dientes:false,
		maximos_beneficios:false,
		blanquea:false,
		tipo:'whitening',
		sabor:'Menta',
		color:'#4b4c9b',
		color_2:'#4b4c9b',
		bitly:'http://bit.ly/1bvgkSL',
		link_exito:'http://www.exito.com/products/0000472243684721/Original+pet?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Solucion',
		link_la14:'http://www.la14.com/Tiendala14/Products/base_catalog/PID-499.aspx?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Solucion',
		link_larebaja:'http://www.larebajavirtual.com/catalogo/index/verproducto/producto/LISTERINE-ORIGINAL/productoId/14619/divId?utm_source=website&utm_medium=LISTERINE.co&utm_content=preCommerce&utm_campaign=Johnson-and-Johnson&utm_term=Listerine-Solucion'
	}
	];

	$scope.getInfo = function(idP){
		var obj = $filter('filter')($scope.tipos, {id:idP}, true)[0];
		return obj;
	};

	$scope.getDivHeight = function(id){
		var h=$('#'+id).height();
		return h+"px";
	}

	$scope.go = function ( path ) {
		$location.path( path );
	};

	$scope.active;

	$scope.showThumb = function(id){
		$scope.active=id;
		return $scope.active;
	};

	$scope.absUrl = $location.absUrl();

	$scope.$on('background.color.change', function(event, bgClass) {
		$scope.current_background = bgClass;
	});

	$scope.changeBgColor = function(color){
		$scope.$emit('background.color.change', color);
	}

	$rootScope.$on('$routeChangeSuccess', function () {
		console.log($location.$$path);
		switch($location.$$path){
			case '/':
			$scope.changeBgColor('purpleBg');
			setTitle();
			break;
			case '/que-estas-buscando-en-tu-enjuague-bucal':
			$scope.changeBgColor('purpleBg');
			setTitle();
			break;
			case '/elije-que-sabor-prefieres-en-tu-enjuague-bucal-sin-alcohol':
			$scope.changeBgColor('greenBg');
			setTitle();
			break;
			case '/elije-que-beneficios-buscas-en-tu-enjuague-bucal':
			$scope.changeBgColor('blueBg');
			setTitle();
			break;
			case '/compara-y-descubre-los-beneficios-de-ocho-variedades-de-LISTERINE':
			$scope.changeBgColor('purpleBg');
			setTitle();
			break;
			case '/compara-las-variedades-de-LISTERINE':
			$scope.changeBgColor('purpleBg');
			setTitle();
			break;
			case '/LISTERINE-Whitening-para-blanqueamiento-dental':
			case '/LISTERINE-Whitening-para-blanqueamiento-dental-continue':
			$scope.changeBgColor('lightblueBg');
			changeTitle();
			break;
			case '/compara-y-descubre-los-beneficios-de-las-tres-variedades-de-LISTERINE-Whitening':
			$scope.changeBgColor('purpleBg');
			setTitle();
			break;
			case '/este-es-el-ideal/LISTERINE-solucion/beneficios':
			case '/este-es-el-ideal/LISTERINE-solucion/modo_de_uso':
			$scope.changeBgColor('solucion');
			$scope.pageTitle= '¡Ya encontramos tu LISTERINE® ideal: LISTERINE® Solución!';
			$scope.meta_title='¡Ya encontramos tu LISTERINE® ideal: LISTERINE® Solución!';
			$scope.meta_description='Si quieres tener los mayores beneficios para tu boca, ¡descubre el tuyo!';
			$scope.meta_url='http://www.listerine.co/descubre-el-listerine-ideal-para-ti'+$location.$$path;
			$scope.meta_keywords='elimina el mal aliento, placa bacteriana, encias sanas, listerine';
			setTitle();
			break;
			case '/este-es-el-ideal/LISTERINE-freshburst/beneficios':
			case '/este-es-el-ideal/LISTERINE-freshburst/modo_de_uso':
			$scope.changeBgColor('freshburst');
			$scope.pageTitle= '¡Ya encontramos tu LISTERINE® ideal: LISTERINE® freshburst!';
			$scope.meta_title='¡Ya encontramos tu LISTERINE® ideal: LISTERINE® freshburst!';
			$scope.meta_description='Si quieres tener los mayores beneficios para tu boca, ¡descubre el tuyo!';
			$scope.meta_url='http://www.listerine.co/descubre-el-listerine-ideal-para-ti'+$location.$$path;
			$scope.meta_keywords='elimina el mal aliento, placa bacteriana, encias sanas, listerine';
			setTitle();
			break;
			case '/este-es-el-ideal/LISTERINE-cool-mint/beneficios':
			case '/este-es-el-ideal/LISTERINE-cool-mint/modo_de_uso':
			$scope.changeBgColor('cool-mint');
			setTitle();
			break;
			case '/este-es-el-ideal/LISTERINE-zero-menta-verde/beneficios':
			case '/este-es-el-ideal/LISTERINE-zero-menta-verde/modo_de_uso':
			$scope.changeBgColor('zero-menta-verde');
			setTitle();
			break;
			case '/este-es-el-ideal/LISTERINE-zero/beneficios':
			case '/este-es-el-ideal/LISTERINE-zero/modo_de_uso':
			$scope.changeBgColor('zero');
			setTitle();
			break;
			case '/este-es-el-ideal/LISTERINE-control-calculo/beneficios':
			case '/este-es-el-ideal/LISTERINE-control-calculo/modo_de_uso':
			$scope.changeBgColor('control-calculo');
			setTitle();
			break;
			case '/este-es-el-ideal/LISTERINE-proteccion-dyt/beneficios':
			case '/este-es-el-ideal/LISTERINE-proteccion-dyt/modo_de_uso':
			$scope.changeBgColor('proteccion-dyt');
			setTitle();
			break;
			case '/este-es-el-ideal/LISTERINE-cuidado-total/beneficios':
			case '/este-es-el-ideal/LISTERINE-cuidado-total/modo_de_uso':
			$scope.changeBgColor('cuidado-total');
			setTitle();
			break;
			case '/este-es-el-ideal/LISTERINE-whitening-precepillado/beneficios':
			case '/este-es-el-ideal/LISTERINE-whitening-precepillado/modo_de_uso':
			$scope.changeBgColor('whitening-precepillado');
			setTitle();
			break;
			case '/este-es-el-ideal/LISTERINE-whitening-2en1/beneficios':
			case '/este-es-el-ideal/LISTERINE-whitening-2en1/modo_de_uso':
			$scope.changeBgColor('whitening-2en1');
			setTitle();
			break;
			case '/este-es-el-ideal/LISTERINE-whitening-antimanchas/beneficios':
			case '/este-es-el-ideal/LISTERINE-whitening-antimanchas/modo_de_uso':
			$scope.changeBgColor('whitening-antimanchas');
			setTitle();
			case '/este-es-el-ideal/LISTERINE-whitening-extreme/beneficios':
			case '/este-es-el-ideal/LISTERINE-whitening-extreme/modo_de_uso':
			$scope.changeBgColor('lightblueBg');
			setTitle();
			break;
		}
	});

function changeTitle(){
	$('#ayuda-titulo').html("DESCUBRE LOS BENEFICIOS");
	$('#consejo-titulo').html("de <strong>LISTERINE<sup>®</sup></strong> Whitening");
}

function setTitle(){
	$('#ayuda-titulo').html("¡AYUDA! ¿CUÁL ESCOJO?");
	$('#consejo-titulo').html("Hay un <strong>LISTERINE<sup>®</sup></strong> ideal para ti");
}




}]);

app.directive('ngShare', function() {
	return {
		restrict: 'A',
		require: '^ngSocialId',
		scope: {
			ngSocialId: '@'
		},
		templateUrl: 'templates/social-share.html'
	}
});

app.controller('ProductController', ['$scope', '$routeParams','$filter',function($scope, $routeParams, $filter) {
	$scope.productId = $routeParams.productId;
	$scope.estado = $routeParams.estado;

	if($scope.estado=="beneficios"){
		$('ul.tabs').tabs('select_tab', 'beneficios');
	}
	if($scope.estado=="modo_de_uso"){
		$('ul.tabs').tabs('select_tab', 'modoUso');
	}
	$scope.productObj = $filter('filter')($scope.tipos, {id:$scope.productId}, true)[0];

}]);


app.config(function($routeProvider, $locationProvider){

	$routeProvider

	.when('/',{
		templateUrl:"public/pages/descubre-el-listerine-ideal-para-ti.html"
	})
	.when('/que-estas-buscando-en-tu-enjuague-bucal',{
		templateUrl:"public/pages/que-estas-buscando-en-tu-enjuague-bucal.html"
	})
	.when('/elije-que-sabor-prefieres-en-tu-enjuague-bucal-sin-alcohol',{
		templateUrl:"public/pages/elije-que-sabor-prefieres-en-tu-enjuague-bucal-sin-alcohol.html"
	})
	.when('/elije-que-beneficios-buscas-en-tu-enjuague-bucal',{
		templateUrl:"public/pages/elije-que-beneficios-buscas-en-tu-enjuague-bucal.html"
	})
	.when('/este-es-el-ideal/:productId/:estado',{
		templateUrl:"public/pages/este-es-el-ideal.html",
		controller: 'ProductController'
	})
	.when('/compara-y-descubre-los-beneficios-de-ocho-variedades-de-LISTERINE',{
		templateUrl:"public/pages/compara-y-descubre-los-beneficios-de-ocho-variedades-de-LISTERINE.html"
	})
	.when('/compara-las-variedades-de-LISTERINE',{
		templateUrl:"public/pages/compara-las-variedades-de-LISTERINE.html"
	})
	.when('/LISTERINE-Whitening-para-blanqueamiento-dental',{
		templateUrl:"public/pages/LISTERINE-Whitening-para-blanqueamiento-dental.html"
	})
	.when('/LISTERINE-Whitening-para-blanqueamiento-dental-continue',{
		templateUrl:"public/pages/LISTERINE-Whitening-para-blanqueamiento-dental-continue.html"
	})
	.when('/compara-y-descubre-los-beneficios-de-las-tres-variedades-de-LISTERINE-Whitening',{
		templateUrl:"public/pages/compara-y-descubre-los-beneficios-de-las-tres-variedades-de-LISTERINE-Whitening.html",
	})
	.otherwise({
		templateUrl:"public/pages/que-estas-buscando-en-tu-enjuague-bucal.html"
	});

	$locationProvider.html5Mode({
		enabled: true,
	 // requireBase: false
	});

});

app.run(function ($rootScope, $location,$route, $timeout) {
 $rootScope.facebookAppId = '591947097496965'; // set your facebook app id here
 var history = [];

 $rootScope.back = function () {
 	var prevUrl = history.length > 1 ? history.splice(-2)[0] : "/";
 	$location.path(prevUrl);
 };

 $rootScope.config = {};
 $rootScope.config.app_url = $location.url();
 $rootScope.config.app_path = $location.path();
 $rootScope.layout = {};
 $rootScope.layout.loading = false;

 $rootScope.$on('$routeChangeStart', function () {
 	console.log('$routeChangeStart');
        //show loading gif
        $timeout(function(){
        	$rootScope.layout.loading = true;
        });
    });
 $rootScope.$on('$routeChangeSuccess', function () {
 	console.log('$routeChangeSuccess');
 	history.push($location.$$path);

 	$rootScope.rutaActual = $location.$$path;
        //hide loading gif
        $timeout(function(){
        	$rootScope.layout.loading = false;
        }, 200);
    });
 $rootScope.$on('$routeChangeError', function () {

 	$rootScope.layout.loading = false;

 });

});